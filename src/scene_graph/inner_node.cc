#include "scene_graph/inner_node.h"

#include <memory>
#include <string>
#include <vector>

#include <glm/glm.hpp>

#include "scene_graph/node.h"

InnerNode::InnerNode(std::vector<std::unique_ptr<Node>> nodes, 
                     glm::mat4 local_coords, 
                     glm::mat4 parent_coords, 
                     std::string name)
    : Node(local_coords, parent_coords, name),
      nodes_(std::move(nodes)) {}

InnerNode::InnerNode(glm::mat4 local_coords, 
                     glm::mat4 parent_coords,
                     std::string name)
    : Node(local_coords, parent_coords, name), nodes_() {}

void InnerNode::Add(std::unique_ptr<Node> node) {
    node->set_parent_coords(parent_coords_);
    nodes_.push_back(std::move(node));
}

void InnerNode::Draw() const {
    for (const auto &node: nodes_) {
        node->Draw();
    }
}

void InnerNode::Update(glm::mat4 matrix) {
    this->Node::Update(matrix);
    for (const auto &node: nodes_) {
        node->Update(matrix);
    }
}

void InnerNode::UpdateNode(glm::mat4 matrix, size_t index) {
    nodes_[index]->Update(matrix);
}

void InnerNode::UpdateNode(glm::mat4 matrix, std::string name) {
    if (name_ == name) {
        Update(matrix);
    } else {
        for (const auto &node: nodes_) {
            if (node->name() == name) {
                node->Update(matrix);
            } else if (auto inner_node = dynamic_cast<InnerNode*>(node.get())) {
                inner_node->UpdateNode(matrix, name);
            }
        }
    }
}
