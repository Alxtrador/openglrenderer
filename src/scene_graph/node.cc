#include "scene_graph/node.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Node::Node(glm::mat4 local_coords, glm::mat4 parent_coords, std::string name)
    : local_coords_(local_coords), parent_coords_(parent_coords), 
      global_coords_(glm::mat4(1.0f)), name_(name) {}

void Node::Draw() const { }

void Node::Update(glm::mat4 matrix) {
    local_coords_ = matrix * local_coords_;
    global_coords_ = parent_coords_ * local_coords_;
}

void Node::set_parent_coords(glm::mat4 parent_coords) {
    parent_coords_ = parent_coords;
}

std::string Node::name() {
    return name_;
}
