#include "scene_graph/geometry_node.h"

#include <GL/glew.h>

#include <memory>
#include <vector>

#include <glm/glm.hpp>

#include "material.h"
#include "mesh.h"

GeometryNode::GeometryNode(std::shared_ptr<Mesh> mesh, 
                           std::shared_ptr<Material> material, 
                           glm::mat4 local_coords, 
                           glm::mat4 parent_coords, 
                           std::string name)
    : Node(local_coords, parent_coords, name), 
      mesh_(mesh), material_(material) {}

void GeometryNode::Draw() const {
    glBindVertexArray(mesh_->vertex_array_object());
    material_->ApplyMaterial();

    GLuint program = material_->shader_program()->program();
    GLint model_matrix = glGetUniformLocation(program, "modelMatrix");
    glUniformMatrix4fv(model_matrix, 1, GL_FALSE, &global_coords_[0][0]);
    
    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh_->index_size()), GL_UNSIGNED_INT, 0);
}