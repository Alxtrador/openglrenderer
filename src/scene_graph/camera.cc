#include "scene_graph/camera.h"

#include <GL/glew.h>

#include <memory>

#include <glm/glm.hpp>
#include <glm/ext/matrix_clip_space.hpp>

Camera::Camera(std::shared_ptr<ShaderProgram> shader_program, 
              int width, int height, 
              glm::mat4 local_coords, 
              glm::mat4 parent_coords, 
              std::string name)
    : Node(local_coords, parent_coords, name),
      shader_program_(shader_program),
      projection_(InitProjection(width, height)) {}

void Camera::Draw() const {
    GLuint program = shader_program_->program();
    glUseProgram(program);
    GLint projection_matrix = glGetUniformLocation(program, "projectionMatrix");
    glUniformMatrix4fv(projection_matrix, 1, GL_FALSE, &projection_[0][0]);
    GLint view_matrix = glGetUniformLocation(program, "viewMatrix");
    glUniformMatrix4fv(view_matrix, 1, GL_FALSE, &global_coords_[0][0]);
}

glm::mat4 Camera::InitProjection(int width, int height) {
    return glm::perspective(90.0f, static_cast<float>(width) / static_cast<float>(height), 0.01f, 100.0f);
}