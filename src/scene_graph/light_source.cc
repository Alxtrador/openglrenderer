#include "scene_graph/light_source.h"

#include <GL/glew.h>

#include "shader_program.h"

LightSource::LightSource(glm::mat4 local_coords,
                         glm::mat4 parent_coords,
                         glm::vec4 light_diffuse,
                         glm::vec4 light_specular,
                         glm::vec4 light_ambient,
                         float attenuation_constant,
                         float attenuation_linear,
                         float attenuation_quadratic,
                         glm::vec4 light_position,
                         std::shared_ptr<ShaderProgram> shader_program, 
                         std::string name)
    : Node(local_coords, parent_coords, name), 
      light_diffuse_(light_diffuse), 
      light_specular_(light_specular), 
      light_ambient_(light_ambient), 
      attenuation_constant_(attenuation_constant), 
      attenuation_linear_(attenuation_linear), 
      attenuation_quadratic_(attenuation_quadratic),
      light_position_(light_position),
      global_light_position_(light_position),
      shader_program_(shader_program) {}

void LightSource::Draw() const {
    GLuint program = shader_program_->program();
    glUseProgram(program);
    GLint ambient = glGetUniformLocation(program, "lightAmbient");
    glUniform4fv(ambient, 1, &light_ambient_[0]);
    GLint diffuse = glGetUniformLocation(program, "lightDiffuse");
    glUniform4fv(diffuse, 1, &light_diffuse_[0]);
    GLint specular = glGetUniformLocation(program, "lightSpecular");
    glUniform4fv(specular, 1, &light_specular_[0]);
    GLint position = glGetUniformLocation(program, "lightPosition");
    glUniform4fv(position, 1, &global_light_position_[0]);
}

void LightSource::Update(glm::mat4 matrix) {
    local_coords_ = matrix * local_coords_;
    global_coords_ = parent_coords_ * local_coords_;
    global_light_position_ = global_coords_ * light_position_;
}