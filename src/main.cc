#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "monochrome_material.h"
#include "scene_graph/camera.h"
#include "scene_graph/geometry_node.h"
#include "scene_graph/inner_node.h"
#include "scene_graph/light_source.h"
#include "scene_graph/node.h"
#include "shapes/cube.h"
#include "shapes/cylinder.h"
#include "shapes/pyramid.h"
#include "shapes/sphere.h"
#include "shapes/triangle.h"
#include "user_defined_material.h"

glm::mat4 RotateX(float angle) {
    glm::quat rotation = glm::angleAxis(angle, glm::vec3(1.0f, 0.0f, 0.0f));
    return glm::mat4_cast(rotation);
}

glm::mat4 RotateY(float angle) {
    glm::quat rotation = glm::angleAxis(angle, glm::vec3(0.0f, 1.0f, 0.0f));
    return glm::mat4_cast(rotation);
}

glm::mat4 RotateZ(float angle) {
    glm::quat rotation = glm::angleAxis(angle, glm::vec3(0.0f, 0.0f, 1.0f));
    return glm::mat4_cast(rotation);
}

glm::mat4 HandleInput(GLFWwindow* window, glm::mat4 translate) {
    glm::mat4 new_translate = translate;

    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    } else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, 0.1f, 0.0f});
    } else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        new_translate = glm::translate(translate,  glm::vec3 {0.1f, 0.0f, 0.0f});
    } else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, -0.1f, 0.0f});
    } else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        new_translate = glm::translate(translate,  glm::vec3 {-0.1f, 0.0f, 0.0f});
    } else if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, 0.0f, -0.1f});
    } else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, 0.0f, 0.1f});
    }

    return new_translate;
}

glm::mat4 HandleInputCamera(GLFWwindow* window) {
    glm::mat4 rotation = glm::mat4(1.0f);

    if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
        rotation = RotateX(-0.03f);
    }
    if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
        rotation = RotateX(0.03f);
    }
    if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
        rotation = RotateY(-0.03f);
    }
    if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
        rotation = RotateY(0.03f);
    }
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
        rotation = RotateZ(-0.03f);
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
        rotation = RotateZ(0.03f);
    }
    return rotation;
}

glm::mat4 ImguiTranslateWindow(glm::mat4 translate, const char* title) {
    glm::mat4 new_translate = translate;
    bool t = true;
    ImGui::Begin(title, &t);
    if (ImGui::Button("Up")) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, 0.1f, 0.0f});
    }
    if (ImGui::Button("Down")) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, -0.1f, 0.0f});
    }
    if (ImGui::Button("Right")) {
        new_translate = glm::translate(translate,  glm::vec3 {0.1f, 0.0f, 0.0f});
    }
    if (ImGui::Button("Left")) {
        new_translate = glm::translate(translate,  glm::vec3 {-0.1f, 0.0f, 0.0f});
    }
    if (ImGui::Button("Forward")) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, 0.0f, -0.1f});
    }
    if (ImGui::Button("Backward")) {
        new_translate = glm::translate(translate,  glm::vec3 {0.0f, 0.0f, 0.1f});
    }
    ImGui::End();
    return new_translate;
}

glm::mat4 ImguiRotateWindow(const char* title) {
    glm::mat4 rotation = glm::mat4(1.0f);
    bool t = true;
    ImGui::Begin(title, &t);
    if (ImGui::Button("Up")) {
        rotation = RotateX(-0.05f);
    }
    if (ImGui::Button("Down")) {
        rotation = RotateX(0.05f);
    }
    if (ImGui::Button("Right")) {
        rotation = RotateY(-0.05f);
    }
    if (ImGui::Button("Left")) {
        rotation = RotateY(0.05f);
    }
    if (ImGui::Button("Sideways Right")) {
        rotation = RotateZ(-0.05f);
    }
    if (ImGui::Button("Sideways Left")) {
        rotation = RotateZ(0.05f);
    }
    ImGui::End();
    return rotation;
}

std::string read_shader(std::string file_name) {
    std::string result;
    std::string line;
    std::ifstream file (file_name);
    if (file.is_open()) {
        while (getline(file, line)) {
            result.append(line);
            result.append("\n");
        }
        file.close();
    }
    return result;
}

std::shared_ptr<ShaderProgram> InitShaderProgram() {
    std::string vertex_shader_string = read_shader("shaders/vertex.glsl");
    std::string fragment_shader_string = read_shader("shaders/fragment.glsl");
    std::shared_ptr<Shader> vertex_shader = std::make_shared<Shader>(kVertex, vertex_shader_string.c_str());
    std::shared_ptr<Shader> fragment_shader = std::make_shared<Shader>(kFragment, fragment_shader_string.c_str());
    std::array<std::shared_ptr<Shader>, 2> shaders = {vertex_shader, fragment_shader};
    std::shared_ptr<ShaderProgram> shader_program = std::make_shared<ShaderProgram>(shaders);
    return shader_program;
}

std::unique_ptr<Camera> InitCamera(std::shared_ptr<ShaderProgram> shader_program, glm::mat4 parent_coords, int width, int height) {
    glm::vec3 eye = glm::vec3(0.0f, 0.0f, 3.0f);
    glm::vec3 centre = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::mat4 view = glm::lookAt(eye, centre, up);
    return std::make_unique<Camera>(shader_program, width, height, view, parent_coords, "camera");
}

std::unique_ptr<LightSource> InitLight(std::shared_ptr<ShaderProgram> shader_program, 
                                 glm::mat4 local_coords, glm::mat4 parent_coords,
                                 glm::vec4 diffuse, glm::vec4 specular, glm::vec4 ambient, glm::vec4 light_position) {
    return std::make_unique<LightSource>(local_coords, parent_coords, diffuse, specular, ambient, 0.0f, 0.0f, 0.0f, light_position, shader_program, "light");
}

std::unique_ptr<InnerNode> CreateFloor(std::shared_ptr<ShaderProgram> shader_program, int size) {
    glm::mat4 ident = glm::mat4(1.0f);
    std::unique_ptr<InnerNode> floor = std::make_unique<InnerNode>(ident, ident, "floor");
    std::shared_ptr<Triangle> triangle_mesh = std::make_shared<Triangle>();
    std::shared_ptr<MonochromeMaterial> material = std::make_shared<MonochromeMaterial>(shader_program);
    std::shared_ptr<UserDefinedMaterial> alternate_material = std::make_shared<UserDefinedMaterial>(shader_program, 
        glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), 30.0f);
    glm::mat4 rotation = RotateY(3.1415f);

    for (int x = 0; x < size; ++x) {
        for (int z = 0; z < size; ++z) {
            floor->Add(std::make_unique<GeometryNode>(triangle_mesh, material, glm::translate(ident,  glm::vec3(-2.0 + x, -2.0, 0.0 + z)), ident, "floor_tile_0"));
            floor->Add(std::make_unique<GeometryNode>(triangle_mesh, alternate_material, glm::translate(ident,  glm::vec3(-1.0 + x, -2.0, 1.0 + z)) * rotation, ident, "floor_tile_0"));
        }
    }
    return floor;
}

std::unique_ptr<InnerNode> InitNodes(std::shared_ptr<ShaderProgram> shader_program, int width, int height) {
    std::shared_ptr<Cube> cube_mesh = std::make_shared<Cube>();
    std::shared_ptr<Sphere> sphere_mesh = std::make_shared<Sphere>(5);
    std::shared_ptr<Cylinder> cylinder_mesh = std::make_shared<Cylinder>(5);
    std::shared_ptr<Pyramid> pyramid_mesh = std::make_shared<Pyramid>(5);
    std::shared_ptr<MonochromeMaterial> material = std::make_shared<MonochromeMaterial>(shader_program);

    glm::vec4 diffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    glm::vec4 specular = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    glm::vec4 ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    glm::vec4 light_position = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

    glm::mat4 ident = glm::mat4(1.0f);
    glm::mat4 parent_coords = glm::mat4(1.0f);

    std::unique_ptr<InnerNode> scene_nodes = std::make_unique<InnerNode>(parent_coords, parent_coords, "person");
    //Lower body
    std::unique_ptr<InnerNode> lower_body = std::make_unique<InnerNode>(parent_coords, parent_coords, "lower_body");
    //Right Leg
    std::unique_ptr<InnerNode> right_leg = std::make_unique<InnerNode>(parent_coords, parent_coords, "right_leg");
    right_leg->Add(std::make_unique<GeometryNode>(cylinder_mesh, material, glm::translate(ident,  glm::vec3(-0.2, -1.0, -0.8)), parent_coords, "right_leg_cube"));
    right_leg->UpdateNode(glm::scale(glm::vec3(0.3f, 1.5f, 0.2f)), 0);
    right_leg->Add(std::make_unique<GeometryNode>(pyramid_mesh, material, glm::translate(ident,  glm::vec3(0.0, -3.6, 0.0)), parent_coords, "right_leg_pyramid"));
    right_leg->UpdateNode(glm::scale(glm::vec3(1.0f, 0.5f, 1.0f)), 1);
    //Left Leg
    std::unique_ptr<InnerNode> left_leg = std::make_unique<InnerNode>(parent_coords, parent_coords, "left_leg");
    left_leg->Add(std::make_unique<GeometryNode>(cylinder_mesh, material, glm::translate(ident,  glm::vec3(2.5, -1.0, -0.8)), parent_coords, "left_leg_cube"));
    left_leg->UpdateNode(glm::scale(glm::vec3(0.3f, 1.5f, 0.2f)), 0);
    left_leg->Add(std::make_unique<GeometryNode>(pyramid_mesh, material, glm::translate(ident,  glm::vec3(0.5, -3.6, 0.0)), parent_coords, "left_leg_pyramid"));
    left_leg->UpdateNode(glm::scale(glm::vec3(1.0f, 0.5f, 1.0f)), 1);

    lower_body->Add(std::move(left_leg));
    lower_body->Add(std::move(right_leg));
    //Upper body
    std::unique_ptr<InnerNode> upper_body = std::make_unique<InnerNode>(parent_coords, parent_coords, "upper_body");
    //Torso
    std::unique_ptr<InnerNode> torso = std::make_unique<InnerNode>(parent_coords, parent_coords, "torso");
    torso->Add(std::make_unique<GeometryNode>(cube_mesh, material, glm::translate(ident,  glm::vec3(-0.1, 0.0, -0.2)), parent_coords, "torso_cube"));
    torso->UpdateNode(glm::scale(glm::vec3(2.5f, 3.0f, 0.5f)), 0);
    //Right Arm
    std::unique_ptr<InnerNode> right_arm = std::make_unique<InnerNode>(parent_coords, parent_coords, "right_arm");
    right_arm->Add(std::make_unique<GeometryNode>(cube_mesh, material, glm::translate(ident,  glm::vec3(0.2, 1.3, -0.16)), parent_coords, "right_arm_cube"));
    right_arm->UpdateNode(glm::scale(glm::vec3(4.0f, 1.0f, 0.5f)), 0);
    //Left Arm
    std::unique_ptr<InnerNode> left_arm = std::make_unique<InnerNode>(parent_coords, parent_coords, "left_arm");
    left_arm->Add(std::make_unique<GeometryNode>(cube_mesh, material, glm::translate(ident,  glm::vec3(-0.5, 1.3, -0.16)), parent_coords, "left_arm_cube"));
    left_arm->UpdateNode(glm::scale(glm::vec3(4.0f, 1.0f, 0.5f)), 0);
    //Head
    std::unique_ptr<InnerNode> head = std::make_unique<InnerNode>(parent_coords, parent_coords, "head");
    head->Add(std::make_unique<GeometryNode>(sphere_mesh, material, glm::translate(ident,  glm::vec3(0.4, 2.7, -0.2)), parent_coords, "head_sphere"));
    head->UpdateNode(glm::scale(glm::vec3(0.8f, 0.8f, 0.5f)), 0);
    //Floor
    std::unique_ptr<InnerNode> floor = CreateFloor(shader_program, 5);

    upper_body->Add(std::move(torso));
    upper_body->Add(std::move(right_arm));
    upper_body->Add(std::move(left_arm));
    upper_body->Add(std::move(head));

    scene_nodes->Add(std::move(lower_body));
    scene_nodes->Add(std::move(upper_body));

    std::unique_ptr<InnerNode> scene = std::make_unique<InnerNode>(parent_coords, parent_coords, "scene");
    scene->Add(InitCamera(shader_program, parent_coords, width, height));
    scene->Add(InitLight(shader_program, glm::translate(ident,  glm::vec3 {1.0, 1.0, 1.0}), parent_coords, diffuse, specular, ambient, light_position));
    scene->Add(std::move(scene_nodes));
    scene->Add(std::move(floor));

    return scene;
}

int main() {
    //Setup GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    const int width = 1024;
    const int height = 768;
    GLFWwindow* window = glfwCreateWindow(width, height, "Renderer", NULL, NULL);
    glfwMakeContextCurrent(window);

    //Setup GLEW and OpenGL
    glewExperimental = GL_TRUE; 
    glewInit();
    glViewport(0, 0, width, height);
    glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //Setup Dear ImGui
    ImGui::CreateContext();
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330");

    //Setup scene graph
    std::shared_ptr<ShaderProgram> shader_program = InitShaderProgram();
    std::unique_ptr<InnerNode> nodes = InitNodes(shader_program, width, height);

    //Setup translation and rotation matrices
    glm::mat4 translate = glm::mat4(1.0f);
    glm::mat4 scene_translate = glm::mat4(1.0f);
    glm::mat4 ident = glm::mat4(1.0f);
    glm::mat4 camera_rotation = glm::mat4(1.0f);
    glm::mat4 person_rotation = glm::mat4(1.0f);
    glm::mat4 upper_body_rotation = glm::mat4(1.0f);
    glm::mat4 head_rotation = glm::mat4(1.0f);

    while (!glfwWindowShouldClose(window)) {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        scene_translate = ImguiTranslateWindow(ident, "Control scene graph");
        upper_body_rotation = ImguiRotateWindow("Rotate Upper Body");
        head_rotation = ImguiRotateWindow("Rotate Head");
        person_rotation = ImguiRotateWindow("Rotate Person");
        ImGui::Render();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        translate = HandleInput(window, translate);
        camera_rotation = HandleInputCamera(window);
        nodes->UpdateNode(camera_rotation, "camera");
        nodes->UpdateNode(translate, "camera");
        nodes->UpdateNode(upper_body_rotation, "upper_body");
        nodes->UpdateNode(scene_translate, "scene");
        nodes->UpdateNode(head_rotation, "head");
        nodes->UpdateNode(person_rotation, "person");
        nodes->Update(ident);
        nodes->Draw();
        translate = glm::mat4(1.0f);

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
