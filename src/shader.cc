#include "shader.h"

#include <GL/glew.h>

#include <iostream>

Shader::Shader(ShaderType shader_type, const char* shader_source)
    : shader_handle_(CreateShaderHandle(shader_type, shader_source)), shader_type_(shader_type) {}

GLuint Shader::CreateShaderHandle(ShaderType shader_type, const char* shader_source) {
    GLuint shader;
    if (shader_type == kVertex) {
        std::cout << "Creating vertex shader\n";
        shader = glCreateShader(GL_VERTEX_SHADER);
    } else {
        std::cout << "Creating fragment shader\n";
        shader = glCreateShader(GL_FRAGMENT_SHADER);
    }
    
    glShaderSource(shader, 1, &shader_source, NULL);
    glCompileShader(shader);

    GLint status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        std::cout << "Issue compiling shader!\n";
    } else if (status == GL_TRUE) {
        std::cout << "Shader compiled!\n";
    }

    return shader;
}

GLuint Shader::shader_handle() {
    return shader_handle_;
}

ShaderType Shader::shader_type() {
    return shader_type_;
}