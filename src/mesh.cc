#include "mesh.h"

#include <GL/glew.h>

#include <vector>
#include <iostream>

#include <glm/glm.hpp>

Mesh::Mesh(std::vector<glm::vec4> vertices, std::vector<glm::vec4> normals,
        std::vector<int> indices)
    : vertex_array_object_(InitVertexArrayObject()), 
      vertex_buffer_object_(InitVertexBufferObject(vertices)),
      normal_buffer_object_(InitNormalBufferObject(normals)),
      index_buffer_object_(InitIndexBufferObject(indices)),
      index_size_(indices.size()) {}

GLuint Mesh::InitVertexArrayObject() {
    GLuint vertex_array_object = 0;
    glGenVertexArrays(1, &vertex_array_object);
    glBindVertexArray(vertex_array_object);
    return vertex_array_object;
}

GLuint Mesh::InitVertexBufferObject(std::vector<glm::vec4> vertices) {
    GLuint vertex_buffer_object = 0;
    glCreateBuffers(1, &vertex_buffer_object);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec4), 
        &vertices[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
    return vertex_buffer_object;
}

GLuint Mesh::InitNormalBufferObject(std::vector<glm::vec4> normals) {
    GLuint normal_buffer_object = 0;
    glCreateBuffers(1, &normal_buffer_object);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(glm::vec4), 
        &normals[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
    return normal_buffer_object;
}

GLuint Mesh::InitIndexBufferObject(std::vector<int> indices) {
    GLuint index_buffer_object = 0;
    glCreateBuffers(1, &index_buffer_object);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(int), 
        &indices[0], GL_STATIC_DRAW);
    return index_buffer_object;
}

GLuint Mesh::vertex_array_object() {
    return vertex_array_object_;
}

size_t Mesh::index_size() {
    return index_size_;
}