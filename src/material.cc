#include "material.h"

#include <memory>

#include <glm/glm.hpp>

#include "shader_program.h"

Material::Material(std::shared_ptr<ShaderProgram> shader_program,
                 glm::vec4 ambient,
                 glm::vec4 diffuse,
                 glm::vec4 specular,
                 glm::vec4 emission,
                 float shininess)
    : shader_program_(shader_program), ambient_(ambient), 
      diffuse_(diffuse), specular_(specular), 
      emission_(emission), shininess_(shininess) {}

void Material::ApplyMaterial() { }

std::shared_ptr<ShaderProgram> Material::shader_program() {
    return shader_program_;
}