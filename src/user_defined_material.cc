#include "user_defined_material.h"

#include <memory>

#include <glm/glm.hpp>

#include "shader_program.h"
#include "material.h"

UserDefinedMaterial::UserDefinedMaterial(
                std::shared_ptr<ShaderProgram> shader_program,
                glm::vec4 ambient,
                glm::vec4 diffuse,
                glm::vec4 specular,
                glm::vec4 emission,
                float shininess)
    : Material(shader_program, ambient, diffuse, specular, emission, shininess) {}

void UserDefinedMaterial::ApplyMaterial() {
    GLuint program = shader_program_->program();
    glUseProgram(program);
    GLint material_ambient = glGetUniformLocation(program, "materialAmbient");
    glUniform4fv(material_ambient, 1, &ambient_[0]);
    GLint material_diffuse = glGetUniformLocation(program, "materialDiffuse");
    glUniform4fv(material_diffuse, 1, &diffuse_[0]);
    GLint material_specular = glGetUniformLocation(program, "materialSpecular");
    glUniform4fv(material_specular, 1, &specular_[0]);
    GLint shininess = glGetUniformLocation(program, "shininess");
    glUniform1f(shininess, shininess_);
}
