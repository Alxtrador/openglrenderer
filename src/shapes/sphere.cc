#include "shapes/sphere.h"

#include <vector>

#include <glm/glm.hpp>

#include "shapes/shape.h"

Sphere::Sphere(int divisions)
    :  Sphere(CreateSphereShape(divisions)) {}

Sphere::Sphere(Shape shape)
    : Mesh(shape.vertices, shape.normals, shape.indices) {}


Shape Sphere::CreateSphereShape(int divisions) {
    std::vector<glm::vec4> v {
        glm::vec4(0.0, 0.0, -1.0, 1.0),
        glm::vec4(0.0, 0.942809, 0.333333, 1.0),
        glm::vec4(-0.816497, -0.471405, 0.333333, 1.0),
        glm::vec4(0.816497, -0.471405, 0.333333, 1.0)
    };

    std::vector<glm::vec4> vertices;
	std::vector<glm::vec4> normals;

	Shape temp_shape_a = DivideTriangle(v[0], v[1], v[2], divisions);
	Shape temp_shape_b = DivideTriangle(v[3], v[2], v[1], divisions);
	Shape temp_shape_c = DivideTriangle(v[0], v[3], v[1], divisions);
	Shape temp_shape_d = DivideTriangle(v[0], v[2], v[3], divisions);

    vertices.insert(vertices.end(), temp_shape_a.vertices.begin(), temp_shape_a.vertices.end());
    vertices.insert(vertices.end(), temp_shape_b.vertices.begin(), temp_shape_b.vertices.end());
    vertices.insert(vertices.end(), temp_shape_c.vertices.begin(), temp_shape_c.vertices.end());
    vertices.insert(vertices.end(), temp_shape_d.vertices.begin(), temp_shape_d.vertices.end());
    normals.insert(normals.end(), temp_shape_a.normals.begin(), temp_shape_a.normals.end());
    normals.insert(normals.end(), temp_shape_b.normals.begin(), temp_shape_b.normals.end());
    normals.insert(normals.end(), temp_shape_c.normals.begin(), temp_shape_c.normals.end());
    normals.insert(normals.end(), temp_shape_d.normals.begin(), temp_shape_d.normals.end());

	std::vector<int> indices;
	for (size_t i = 0; i < vertices.size(); ++i) {
		indices.push_back(static_cast<int>(i));
	}

    return Shape { vertices, normals, indices };
}

Shape Sphere::DivideTriangle(glm::vec4 a, glm::vec4 b, glm::vec4 c, int n) {
    std::vector<glm::vec4> vertices;
    std::vector<glm::vec4> normals;
    if (n > 0) {
        glm::vec3 vec3_a = glm::vec3(a);
        glm::vec3 vec3_b = glm::vec3(b);
        glm::vec3 vec3_c = glm::vec3(c);

        glm::vec4 ab = glm::vec4(glm::normalize(glm::mix(vec3_a, vec3_b, 0.5)), 1.0);
        glm::vec4 ac = glm::vec4(glm::normalize(glm::mix(vec3_a, vec3_c, 0.5)), 1.0);
        glm::vec4 bc = glm::vec4(glm::normalize(glm::mix(vec3_b, vec3_c, 0.5)), 1.0);

        Shape temp_shape_a = DivideTriangle(a, ab, ac, n - 1);
        Shape temp_shape_b = DivideTriangle(ab, b, bc, n - 1);
        Shape temp_shape_c = DivideTriangle(bc, c, ac, n - 1);
        Shape temp_shape_d = DivideTriangle(ab, bc, ac, n - 1);

        vertices.insert(vertices.end(), temp_shape_a.vertices.begin(), temp_shape_a.vertices.end());
        vertices.insert(vertices.end(), temp_shape_b.vertices.begin(), temp_shape_b.vertices.end());
        vertices.insert(vertices.end(), temp_shape_c.vertices.begin(), temp_shape_c.vertices.end());
        vertices.insert(vertices.end(), temp_shape_d.vertices.begin(), temp_shape_d.vertices.end());
        normals.insert(normals.end(), temp_shape_a.normals.begin(), temp_shape_a.normals.end());
        normals.insert(normals.end(), temp_shape_b.normals.begin(), temp_shape_b.normals.end());
        normals.insert(normals.end(), temp_shape_c.normals.begin(), temp_shape_c.normals.end());
        normals.insert(normals.end(), temp_shape_d.normals.begin(), temp_shape_d.normals.end());

        std::vector<int> indices;
        return Shape { vertices, normals, indices };
    }
    return Triangle(a, b, c, vertices, normals);
}

Shape Sphere::Triangle(glm::vec4 a, glm::vec4 b, glm::vec4 c, std::vector<glm::vec4> vertices, std::vector<glm::vec4> normals) {
    vertices.push_back(a);
    vertices.push_back(b);
    vertices.push_back(c);

    normals.push_back(glm::vec4(a[0], a[1], a[2], 0));
    normals.push_back(glm::vec4(b[0], b[1], b[2], 0));
    normals.push_back(glm::vec4(c[0], c[1], c[2], 0));

    std::vector<int> indices;
    return Shape { vertices, normals, indices };
}
