#include "shapes/cube.h"

#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>

#include "shapes/shape.h"

Cube::Cube()
    :  Cube(CreateCubeShape()) {}

Cube::Cube(Shape shape)
    : Mesh(shape.vertices, shape.normals, shape.indices) {}

Shape Cube::Face() {
	std::vector<glm::vec4> vertices {
		glm::vec4( 0.5f, 0.5f, 0.0f, 1.0f ),
		glm::vec4( 0.5f, 0.5f, -0.5f, 1.0f ),
		glm::vec4( 0.5f, 0.0f, -0.5f, 1.0f ),
		glm::vec4( 0.5f, 0.5f, 0.0f, 1.0f ),
		glm::vec4( 0.5f, 0.0f, 0.0f, 1.0f ),
		glm::vec4( 0.5f, 0.0f, -0.5f, 1.0f ),
	};
	std::vector<glm::vec4> normals {
		glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f ),
		glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f ),
		glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f ),
		glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f ),
		glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f ),
		glm::vec4( 1.0f, 0.0f, 0.0f, 0.0f ),
	};
	std::vector<int> indices; //leave empty as indices recalculated later on

	return Shape { vertices, normals, indices };
}

std::vector<glm::vec4> Cube::ApplyAllVec4(std::vector<glm::vec4> vectors, glm::mat4 matrix) {
	std::vector<glm::vec4> rotated_vectors;
	for (size_t i = 0; i < vectors.size(); i++) {
		rotated_vectors.push_back(matrix * vectors[i]);
	}
	return rotated_vectors;
}

Shape Cube::CreateCubeShape() {
	Shape right = Face();
	Shape left = Face();
	glm::mat4 mirror_matrix = glm::scale(glm::vec3(-1.0f, 1.0f, 1.0f));
	glm::mat4 translation_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, 0.0f, 0.0f));
	left.vertices = ApplyAllVec4(left.vertices, translation_matrix * mirror_matrix);
	left.normals = ApplyAllVec4(left.normals, mirror_matrix);
	Shape top = Face();
	float angle = 3.1415f/2.0f;
	glm::mat4 rotation_matrix = glm::mat4_cast(glm::angleAxis(angle, glm::vec3(0.0f, 0.0f, 1.0f)));
	top.vertices = ApplyAllVec4(top.vertices, translation_matrix * rotation_matrix);
	top.normals = ApplyAllVec4(top.normals, rotation_matrix);
	Shape bottom = Face();
	angle = 3.1415f/2.0f;
	rotation_matrix = glm::mat4_cast(glm::angleAxis(angle, glm::vec3(0.0f, 0.0f, 1.0f)));
	mirror_matrix = glm::scale(glm::vec3(1.0f, -1.0f, 1.0f));
	translation_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -0.5f, 0.0f));
	bottom.vertices = ApplyAllVec4(bottom.vertices, mirror_matrix * translation_matrix * rotation_matrix);
	bottom.normals = ApplyAllVec4(bottom.normals, mirror_matrix * rotation_matrix);
	Shape front = Face();
	rotation_matrix = glm::mat4_cast(glm::angleAxis(angle, glm::vec3(0.0f, 1.0f, 0.0f)));
	mirror_matrix = glm::scale(glm::vec3(1.0f, 1.0f, -1.0f));
	translation_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, 0.0f, -0.5f));
	front.vertices = ApplyAllVec4(front.vertices, translation_matrix * mirror_matrix * rotation_matrix);
	front.normals = ApplyAllVec4(front.normals, mirror_matrix * rotation_matrix);
	Shape back = Face();
	translation_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, 0.0f, 0.0f));
	back.vertices = ApplyAllVec4(back.vertices, translation_matrix * rotation_matrix);
	back.normals = ApplyAllVec4(back.normals, rotation_matrix);

	std::vector<glm::vec4> vertices;
	vertices.insert(vertices.end(), right.vertices.begin(), right.vertices.end());
	vertices.insert(vertices.end(), left.vertices.begin(), left.vertices.end());
	vertices.insert(vertices.end(), top.vertices.begin(), top.vertices.end());
	vertices.insert(vertices.end(), bottom.vertices.begin(), bottom.vertices.end());
	vertices.insert(vertices.end(), front.vertices.begin(), front.vertices.end());
	vertices.insert(vertices.end(), back.vertices.begin(), back.vertices.end());
	std::vector<glm::vec4> normals;
	normals.insert(normals.end(), right.normals.begin(), right.normals.end());
	normals.insert(normals.end(), left.normals.begin(), left.normals.end());
	normals.insert(normals.end(), top.normals.begin(), top.normals.end());
	normals.insert(normals.end(), bottom.normals.begin(), bottom.normals.end());
	normals.insert(normals.end(), front.normals.begin(), front.normals.end());
	normals.insert(normals.end(), back.normals.begin(), back.normals.end());
	std::vector<int> indices;
	std::cout << vertices.size() << std::endl;
	for (int i = 0; i < static_cast<int>(vertices.size()); ++i) {
		indices.push_back(i);
	}
	return Shape { vertices, normals, indices };
}