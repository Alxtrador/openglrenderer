#include "shapes/triangle.h"

#include <vector>

#include <glm/glm.hpp>

#include "shapes/shape.h"

Triangle::Triangle()
    : Triangle(CreateTriangleShape()) {}

Triangle::Triangle(Shape shape)
    : Mesh(shape.vertices, shape.normals, shape.indices) {}

Shape Triangle::CreateTriangleShape() {
    std::vector<glm::vec4> vertices {
        glm::vec4(0.0, 0.0, 0.0, 1.0),
        glm::vec4(1.0, 0.0, 0.0, 1.0),
        glm::vec4(0.0, 0.0, 1.0, 1.0)
    };
    std::vector<glm::vec4> normals {
        glm::vec4(0.0, 1.0, 0.0, 0.0),
        glm::vec4(0.0, 1.0, 0.0, 0.0),
        glm::vec4(0.0, 1.0, 0.0, 0.0)
    };
    std::vector<int> indices;
	for (int i = 0; i < 36; ++i) {
		indices.push_back(i);
	}
    return Shape { vertices, normals, indices };
}
