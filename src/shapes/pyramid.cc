#include "shapes/pyramid.h"

#include <numbers>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "shapes/shape.h"

constexpr float kPi = std::numbers::pi_v<float>;

Pyramid::Pyramid(int divisions)
    : Pyramid(CreatePyramidShape(divisions)) {}

Pyramid::Pyramid(Shape shape)
    : Mesh(shape.vertices, shape.normals, shape.indices) {}

Shape Pyramid::CreatePyramidShape(int divisions) {
    float radius = 1.0;
    std::vector<glm::vec4> top_vertices;
    std::vector<glm::vec4> bottom_vertices;
    for (int i = 0; i < divisions * 3; ++i) {
        glm::vec4 middle_bottom_point = glm::vec4(0.0, 0.0, 0.0, 1.0);
        bottom_vertices.push_back(middle_bottom_point);

        float angle = (2.0f * kPi) * (static_cast<float>(i)/(static_cast<float>(divisions)*3.0f));
        bottom_vertices.push_back(
            glm::vec4(
                radius * cos(angle),
                0.0,
                radius * sin(angle),
                1.0
            )
        );
        
        angle = (2.0f * kPi) * (static_cast<float>(i+1)/(static_cast<float>(divisions)*3.0f));
        bottom_vertices.push_back(
            glm::vec4(
                radius * cos(angle),
                0.0,
                radius * sin(angle),
                1.0
            )
        );
    }

    std::vector<glm::vec4> vertices;
    std::vector<glm::vec4> normals;
    for (size_t i = 0; i < bottom_vertices.size(); i+=3) {
        float angle = (2.0f * kPi) * (static_cast<float>(i)/(static_cast<float>(bottom_vertices.size())));
		glm::vec4 angled_normal = glm::rotateY(glm::vec4(1.0, 0.0, 0.0, 0.0), angle + 1.5f*kPi);
        angled_normal = glm::normalize(angled_normal);
        float next_angle = (2.0f * kPi) * (static_cast<float>(i+3)/(static_cast<float>(bottom_vertices.size())));
		glm::vec4 more_angled_normal = glm::rotateY(glm::vec4(1.0, 0.0, 0.0, 0.0), next_angle + 1.5f*kPi);
        more_angled_normal = glm::normalize(more_angled_normal);

        vertices.push_back(bottom_vertices[i]);
        vertices.push_back(bottom_vertices[i+1]);
        vertices.push_back(bottom_vertices[i+2]);
        normals.push_back(glm::vec4(0.0, -1.0, 0.0, 0.0));
        normals.push_back(glm::vec4(0.0, -1.0, 0.0, 0.0));
        normals.push_back(glm::vec4(0.0, -1.0, 0.0, 0.0));

        vertices.push_back(glm::vec4(0.0, 1.0, 0.0, 1.0));
        vertices.push_back(bottom_vertices[i+2]);
        vertices.push_back(bottom_vertices[i+1]);
        normals.push_back(glm::vec4(0.0, 1.0, 0.0, 0.0));
        normals.push_back(more_angled_normal);
        normals.push_back(angled_normal);
    }

    std::vector<int> indices;
	for (size_t i = 0; i < vertices.size(); ++i) {
		indices.push_back(static_cast<int>(i));
	}

    return Shape { vertices, normals, indices };
}
