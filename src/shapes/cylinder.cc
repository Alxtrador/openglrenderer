#include "shapes/cylinder.h"

#include <numbers>
#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>

#include "shapes/shape.h"

constexpr float kPi = std::numbers::pi_v<float>;

Cylinder::Cylinder(int divisions)
    : Cylinder(CreateCylinderShape(divisions)) {}

Cylinder::Cylinder(Shape shape)
    : Mesh(shape.vertices, shape.normals, shape.indices) {}

Shape Cylinder::CreateCylinderShape(int divisions) {
    float radius = 1.0;
    std::vector<glm::vec4> top_vertices;
    std::vector<glm::vec4> bottom_vertices;
    //Create top and bottom vertices
    for (int i = 0; i < divisions * 3; ++i) {
        glm::vec4 middle_top_point = glm::vec4(0.0, 1.0, 0.0, 1.0);
        glm::vec4 middle_bottom_point = glm::vec4(0.0, 0.0, 0.0, 1.0);
        top_vertices.push_back(middle_top_point);
        bottom_vertices.push_back(middle_bottom_point);

        float angle = (2.0f * kPi) * (static_cast<float>(i)/(static_cast<float>(divisions)*3.0f));
        top_vertices.push_back(
            glm::vec4(
                radius * cos(angle),
                1.0,
                radius * sin(angle),
                1.0
            )
        );
        bottom_vertices.push_back(
            glm::vec4(
                radius * cos(angle),
                0.0,
                radius * sin(angle),
                1.0
            )
        );
        
        angle = (2.0f * kPi) * (static_cast<float>(i+1)/(static_cast<float>(divisions)*3.0f));
        top_vertices.push_back(
            glm::vec4(
                radius * cos(angle),
                1.0,
                radius * sin(angle),
                1.0
            )
        );
        bottom_vertices.push_back(
            glm::vec4(
                radius * cos(angle),
                0.0,
                radius * sin(angle),
                1.0
            )
        );
    }

    std::vector<glm::vec4> vertices;
    std::vector<glm::vec4> normals;
    //Connect top and bottom ends
    for (size_t i = 0; i < bottom_vertices.size(); i+=3) {
        float angle = (2.0f * kPi) * (static_cast<float>(i)/(static_cast<float>(top_vertices.size())));
		glm::vec4 angled_normal = glm::rotateY(glm::vec4(1.0, 0.0, 0.0, 0.0), angle + 1.5f*kPi);
        angled_normal = glm::normalize(angled_normal);
        float next_angle = (2.0f * kPi) * (static_cast<float>(i+3)/(static_cast<float>(top_vertices.size())));
		glm::vec4 more_angled_normal = glm::rotateY(glm::vec4(1.0, 0.0, 0.0, 0.0), next_angle + 1.5f*kPi);
        more_angled_normal = glm::normalize(more_angled_normal);

        vertices.push_back(top_vertices[i]);
        vertices.push_back(top_vertices[i+1]);
        vertices.push_back(top_vertices[i+2]);
        normals.push_back(glm::vec4(0.0, 1.0, 0.0, 0.0));
        normals.push_back(glm::vec4(0.0, 1.0, 0.0, 0.0));
        normals.push_back(glm::vec4(0.0, 1.0, 0.0, 0.0));

        vertices.push_back(top_vertices[i+1]);
        vertices.push_back(bottom_vertices[i+2]);
        vertices.push_back(bottom_vertices[i+1]);
        normals.push_back(angled_normal);
        normals.push_back(more_angled_normal);
        normals.push_back(angled_normal);

        vertices.push_back(bottom_vertices[i+2]);
        vertices.push_back(top_vertices[i+1]);
        vertices.push_back(top_vertices[i+2]);
        normals.push_back(more_angled_normal);
        normals.push_back(angled_normal);
        normals.push_back(more_angled_normal);

        vertices.push_back(bottom_vertices[i]);
        vertices.push_back(bottom_vertices[i+1]);
        vertices.push_back(bottom_vertices[i+2]);
        normals.push_back(glm::vec4(0.0, -1.0, 0.0, 0.0));
        normals.push_back(glm::vec4(0.0, -1.0, 0.0, 0.0));
        normals.push_back(glm::vec4(0.0, -1.0, 0.0, 0.0));
    }

    std::vector<int> indices;
	for (size_t i = 0; i < vertices.size(); ++i) {
		indices.push_back(static_cast<int>(i));
	}

    return Shape { vertices, normals, indices };
}
