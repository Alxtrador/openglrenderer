#include "monochrome_material.h"

#include <GL/glew.h>

#include <array>
#include <vector>

#include <glm/glm.hpp>

MonochromeMaterial::MonochromeMaterial(std::shared_ptr<ShaderProgram> shader_program)
    : Material(shader_program, 
               glm::vec4(0.33f, 0.22f, 0.03f, 1.0f),
               glm::vec4(0.78f, 0.57f, 0.11f, 1.0f),
               glm::vec4(0.99f, 0.91f, 0.81f, 1.0f), 
               glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), 27.8f) {}

void MonochromeMaterial::ApplyMaterial() {
    GLuint program = shader_program_->program();
    glUseProgram(program);
    GLint material_ambient = glGetUniformLocation(program, "materialAmbient");
    glUniform4fv(material_ambient, 1, &ambient_[0]);
    GLint material_diffuse = glGetUniformLocation(program, "materialDiffuse");
    glUniform4fv(material_diffuse, 1, &diffuse_[0]);
    GLint material_specular = glGetUniformLocation(program, "materialSpecular");
    glUniform4fv(material_specular, 1, &specular_[0]);
    GLint shininess = glGetUniformLocation(program, "shininess");
    glUniform1f(shininess, shininess_);
}