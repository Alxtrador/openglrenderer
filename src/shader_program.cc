#include "shader_program.h"

#include <GL/glew.h>

#include <array>
#include <iostream>
#include <memory>

ShaderProgram::ShaderProgram(std::array<std::shared_ptr<Shader>, 2> shaders)
    : shaders_(shaders), program_(CreateProgram(shaders)) {}

GLuint ShaderProgram::CreateProgram(std::array<std::shared_ptr<Shader>, 2> shaders) {
    GLuint program = glCreateProgram();
    for (const auto &shader: shaders) {
        glAttachShader(program, shader->shader_handle());
    }
    glLinkProgram(program);

    GLint is_linked = 0;
    glGetProgramiv(program,  GL_LINK_STATUS, &is_linked);
    if (is_linked == GL_FALSE) {
        std::cout << "Not linked!\n";
    } else if (is_linked == GL_TRUE) {
        std::cout << "Linked!\n";
    }

    return program;
}

GLuint ShaderProgram::program() {
    return program_;
}