OpenGL Renderer
===============

Requirements
------------
* cmake
* glew
* glfw
* glm
* GCC/Clang

How to compile and run
--------------
Commands to build:
1. cmake .
2. make

Use ./OpenGL_Renderer to run the program
