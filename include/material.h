#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <memory>

#include <glm/glm.hpp>

#include "shader_program.h"

class Material {
    public:
        Material(std::shared_ptr<ShaderProgram> shader_program,
                 glm::vec4 ambient,
                 glm::vec4 diffuse,
                 glm::vec4 specular,
                 glm::vec4 emission,
                 float shininess);
        virtual ~Material() = default;
        virtual void ApplyMaterial() = 0;
        std::shared_ptr<ShaderProgram> shader_program();
        
    protected:
        std::shared_ptr<ShaderProgram> shader_program_;
        glm::vec4 ambient_;
        glm::vec4 diffuse_;
        glm::vec4 specular_;
        glm::vec4 emission_;
        float shininess_;
};

#endif