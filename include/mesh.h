#ifndef MESH_H_
#define MESH_H_

#include <GL/glew.h>

#include <vector>

#include <glm/glm.hpp>

class Mesh {
    public:
        Mesh(std::vector<glm::vec4> vertices, std::vector<glm::vec4> normals, 
             std::vector<int> indices);
        GLuint vertex_array_object();
        size_t index_size();

    private:
        GLuint InitVertexArrayObject();
        GLuint InitVertexBufferObject(std::vector<glm::vec4> vertices);
        GLuint InitNormalBufferObject(std::vector<glm::vec4> normals);
        GLuint InitIndexBufferObject(std::vector<int> indices);

        GLuint vertex_array_object_;
        GLuint vertex_buffer_object_;
        GLuint normal_buffer_object_;
        GLuint index_buffer_object_;
        size_t index_size_;
};

#endif