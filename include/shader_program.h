#ifndef SHADERPROGRAM_H_
#define SHADERPROGRAM_H_

#include <GL/glew.h>

#include <array>
#include <memory>

#include "shader.h"

class ShaderProgram {
    public:
        ShaderProgram(std::array<std::shared_ptr<Shader>, 2> shaders);
        GLuint program();

    private:
        GLuint CreateProgram(std::array<std::shared_ptr<Shader>, 2> shaders);

        std::array<std::shared_ptr<Shader>, 2> shaders_;
        GLuint program_;
};

#endif