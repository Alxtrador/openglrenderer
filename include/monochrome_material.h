#ifndef MONOCHROME_MATERIAL_H_
#define MONOCHROME_MATERIAL_H_

#include <GL/glew.h>

#include <array>

#include "material.h"

class MonochromeMaterial: public Material {
    public:
        MonochromeMaterial(std::shared_ptr<ShaderProgram> shader_program);
        void ApplyMaterial();
};

#endif