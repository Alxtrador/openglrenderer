#ifndef SHADER_H_
#define SHADER_H_

#include <GL/glew.h>

enum ShaderType {
    kVertex = 0,
    kFragment
};

class Shader {
    public:
        Shader(ShaderType shader_type, const char* shader_source);
        GLuint shader_handle();
        ShaderType shader_type(); //for debugging purposes

    private:
        GLuint CreateShaderHandle(ShaderType shader_type, const char* shader_source);
        
        GLuint shader_handle_;
        ShaderType shader_type_;

};

#endif