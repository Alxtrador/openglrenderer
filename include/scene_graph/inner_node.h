#ifndef SCENE_GRAPH_INNER_NODE_H_
#define SCENE_GRAPH_INNER_NODE_H_

#include <memory>
#include <vector>

#include <glm/glm.hpp>

#include "scene_graph/node.h"

class InnerNode: public Node {
    public:
        InnerNode(std::vector<std::unique_ptr<Node>> nodes, 
                  glm::mat4 local_coords, 
                  glm::mat4 parent_coords,
                  std::string name);
        InnerNode(glm::mat4 local_coords, 
                  glm::mat4 parent_coords, 
                  std::string name);
        void Add(std::unique_ptr<Node> node);
        void Draw() const;
        void Update(glm::mat4 matrix);
        void UpdateNode(glm::mat4 matrix, size_t index);
        void UpdateNode(glm::mat4 matrix, std::string name);

    private:
        std::vector<std::unique_ptr<Node>> nodes_;
};

#endif