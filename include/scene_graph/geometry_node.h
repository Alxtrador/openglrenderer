#ifndef SCENE_GRAPH_GEOMETRY_NODE_H_
#define SCENE_GRAPH_GEOMETRY_NODE_H_

#include <memory>

#include <glm/glm.hpp>

#include "material.h"
#include "mesh.h"
#include "node.h"

class GeometryNode: public Node {
    public:
        GeometryNode(std::shared_ptr<Mesh> mesh, 
                     std::shared_ptr<Material> material, 
                     glm::mat4 local_coords, 
                     glm::mat4 parent_coords, 
                     std::string name);
        void Draw() const;
    
    private:
        std::shared_ptr<Mesh> mesh_;
        std::shared_ptr<Material> material_;
};

#endif