#ifndef SCENE_GRAPH_CAMERA_H_
#define SCENE_GRAPH_CAMERA_H_

#include <memory>

#include <glm/glm.hpp>

#include "shader_program.h"
#include "scene_graph/node.h"

class Camera: public Node {
    public:
        Camera(std::shared_ptr<ShaderProgram> shader_program, 
               int width, int height, 
               glm::mat4 local_coords, 
               glm::mat4 parent_coords, 
               std::string name);
        void Draw() const;

    private:
        glm::mat4 InitProjection(int width, int height);

        std::shared_ptr<ShaderProgram> shader_program_;
        glm::mat4 projection_;
};

#endif