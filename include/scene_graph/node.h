#ifndef SCENE_GRAPH_NODE_H_
#define SCENE_GRAPH_NODE_H_

#include <string>

#include <glm/glm.hpp>

class Node {
    public:
        Node(glm::mat4 local_coords, glm::mat4 parent_coords, std::string name);
        virtual ~Node() = default;
        virtual void Draw() const = 0;
        virtual void Update(glm::mat4 matrix);
        void set_parent_coords(glm::mat4 parent_coords);
        std::string name();

    protected:
        glm::mat4 local_coords_;
        glm::mat4 parent_coords_;
        glm::mat4 global_coords_;
        std::string name_; //for searching in scene graph
};

#endif