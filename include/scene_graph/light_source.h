#ifndef SCENE_GRAPH_LIGHT_SOURCE_H_
#define SCENE_GRAPH_LIGHT_SOURCE_H_

#include <memory>

#include <glm/glm.hpp>

#include "scene_graph/node.h"
#include "shader_program.h"

class LightSource: public Node {
    public:
        LightSource(glm::mat4 local_coords,
                    glm::mat4 parent_coords,
                    glm::vec4 light_diffuse,
                    glm::vec4 light_specular,
                    glm::vec4 light_ambient,
                    float attenuation_constant,
                    float attenuation_linear,
                    float attenuation_quadratic,
                    glm::vec4 light_position,
                    std::shared_ptr<ShaderProgram> shader_program,
                    std::string name);
        void Draw() const;
        void Update(glm::mat4 matrix);

    private:
        glm::vec4 light_diffuse_;
        glm::vec4 light_specular_;
        glm::vec4 light_ambient_;
        float attenuation_constant_;
        float attenuation_linear_;
        float attenuation_quadratic_;
        glm::vec4 light_position_;
        glm::vec4 global_light_position_;
        std::shared_ptr<ShaderProgram> shader_program_;
};

#endif