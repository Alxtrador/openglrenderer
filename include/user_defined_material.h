#ifndef USER_DEFINED_MATERIAL_H_
#define USER_DEFINED_MATERIAL_H_

#include "material.h"

class UserDefinedMaterial: public Material {
    public:
        UserDefinedMaterial(std::shared_ptr<ShaderProgram> shader_program,
                            glm::vec4 ambient,
                            glm::vec4 diffuse,
                            glm::vec4 specular,
                            glm::vec4 emission,
                            float shininess);
        void ApplyMaterial();
};

#endif