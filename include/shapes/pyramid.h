#ifndef SHAPES_PYRAMID_H_
#define SHAPES_PYRAMID_H_

#include "mesh.h"

#include "shapes/shape.h"

class Pyramid: public Mesh {
    public:
        Pyramid(int divisions);

    private:
        Pyramid(Shape shape);
        Shape CreatePyramidShape(int divisions);
};

#endif