#ifndef SHAPES_TRIANGLE_H_
#define SHAPES_TRIANGLE_H_

#include "mesh.h"
#include "shapes/shape.h"

class Triangle: public Mesh {
    public:
        Triangle();

    private:
        Triangle(Shape shape);
        Shape CreateTriangleShape();
};

#endif