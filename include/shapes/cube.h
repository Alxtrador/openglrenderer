#ifndef SHAPES_CUBE_H_
#define SHAPES_CUBE_H_

#include <glm/glm.hpp>

#include "mesh.h"
#include "shapes/shape.h"

class Cube: public Mesh {
    public:
        Cube();

    private:
        Cube(Shape shape);
        Shape CreateCubeShape();
        Shape Face();
        std::vector<glm::vec4> ApplyAllVec4(std::vector<glm::vec4> vectors, glm::mat4 matrix);
};

#endif