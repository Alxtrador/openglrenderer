#ifndef SHAPES_SHAPE_H_
#define SHAPES_SHAPE_H_

#include <array>
#include <vector>

#include <glm/glm.hpp>

//template<typename T> using vec = std::array<T, 4>;

struct Shape {
    // std::vector<vec<float>> vertices;
    // std::vector<vec<float>> normals;
    // std::vector<int> indices;
    std::vector<glm::vec4> vertices;
    std::vector<glm::vec4> normals;
    std::vector<int> indices;
};

// template<typename T>
// std::vector<T> flatten(std::vector<glm::vec4> list) {
//     std::vector<T> result;
//     for (const auto &vec: list) {
//         for (auto point: vec) {
//             result.push_back(point);
//         }
//     } 
//     return result;
// }

#endif