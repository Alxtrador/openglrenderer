#ifndef SHAPES_SPHERE_H_
#define SHAPES_SPHERE_H_

#include "mesh.h"

#include <glm/glm.hpp>

#include "shapes/shape.h"

class Sphere: public Mesh {
    public:
        Sphere(int divisions);

    private:
        Sphere(Shape shape);
        Shape CreateSphereShape(int divisions);
        Shape DivideTriangle(glm::vec4 a, glm::vec4 b, glm::vec4 c, int n);
        Shape Triangle(glm::vec4 a, glm::vec4 b, glm::vec4 c, std::vector<glm::vec4> vertices, std::vector<glm::vec4> normals);
};

#endif