#ifndef SHAPES_CYLINDER_H_
#define SHAPES_CYLINDER_H_

#include "mesh.h"

#include "shapes/shape.h"

class Cylinder: public Mesh {
    public:
        Cylinder(int divisions);

    private:
        Cylinder(Shape shape);
        Shape CreateCylinderShape(int divisions);
};

#endif