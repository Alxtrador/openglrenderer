#version 330

in vec4 vPosition;
in vec4 vNormal;
uniform mat4 modelMatrix;
uniform vec4 lightPosition;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 N;
out vec3 L;
out vec3 E;

void main() {
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vPosition;
    N = vNormal.xyz;
    L = lightPosition.xyz - (viewMatrix * modelMatrix * vPosition).xyz;
    if (lightPosition.w == 0.0) L = lightPosition.xyz;
    E = vPosition.xyz;
}