#version 330

uniform vec4 lightAmbient, lightDiffuse, lightSpecular;
uniform vec4 materialAmbient, materialDiffuse, materialSpecular;
uniform float shininess;
in vec3 N;
in vec3 L;
in vec3 E;

void main() {
    //Calculating ambient
    vec4 ambient = lightAmbient * materialAmbient;

    //Calculating diffuse
    vec3 LL = normalize(L);
    vec3 NN = normalize(N);
    float Kd = max(dot(LL, NN), 0.0);
    vec4 diffuse = lightDiffuse * materialDiffuse * Kd;

    //Calculating specular
    vec3 EE = normalize(E);
    vec3 H = normalize(LL+EE);
    float Ks = pow(max(dot(NN, H), 0.0), shininess);
    vec4 specular = Ks * lightSpecular * materialSpecular;
    if (dot(LL, NN) < 0.0) {
        specular = vec4(0.0, 0.0, 0.0, 1.0);
    }

    //Calculating final light
    gl_FragColor = ambient + diffuse + specular;
}